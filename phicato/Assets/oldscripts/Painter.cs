﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Painter : MonoBehaviour {

    //void Update () {
    //	if(((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) || Input.GetMouseButton(0)))
    //       {
    //           Plane objPlane = new Plane(Camera.main.transform.forward * -1, this.transform.position);
    //           Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
    //           float rayDistance;
    //           if(objPlane.Raycast(mRay, out rayDistance))
    //           {
    //               this.transform.position = mRay.GetPoint(rayDistance);
    //           }
    //       }
    //}
    public GameObject Brush;
    [SerializeField] float brushSize = 0.5f;


    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            var Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(Ray, out hit))
            {
                var go = Instantiate(Brush, hit.point + Vector3.up * 0.1f, Quaternion.identity, transform);
                go.transform.localScale = Vector3.one * brushSize;
                go.transform.localRotation = Quaternion.identity;
            }
        }
    }


}
