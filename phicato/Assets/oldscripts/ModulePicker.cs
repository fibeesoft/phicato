﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModulePicker : MonoBehaviour {

    GManager gmanager;

    public void ChooseTheModule()
    {
        gmanager = GameObject.Find("GameManager").GetComponent<GManager>();
        string moduleName = this.name;
        gmanager.ChooseModule(moduleName);
    }
}
