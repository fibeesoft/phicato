﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Errors : MonoBehaviour
{
    [SerializeField] GameObject underConstructionObject;
    bool underConstractionIsActive;

    void Start()
    {

        underConstractionIsActive = false;
        UnderConstructionShow();

    }

    public void UnderConstructionShow()
    {
        if(underConstractionIsActive == true)
        {
            underConstructionObject.SetActive(true);
            underConstractionIsActive = !underConstractionIsActive;
        }
        else
        {
            underConstructionObject.SetActive(false);
            underConstractionIsActive = !underConstractionIsActive;
        }
    }
}
