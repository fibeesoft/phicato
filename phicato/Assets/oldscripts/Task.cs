﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "NewTask")]
public class Task : ScriptableObject
{

    [SerializeField] string title;
    [TextArea(6, 8)] [SerializeField] string question;
    [TextArea(2, 8)] [SerializeField] string answer;
    [TextArea(2, 8)] [SerializeField] string hint;
    [SerializeField] Sprite questionImage, hintImage;



    public string GetQuestion()
    {
        return question;
    }

    public string GetAnswer()
    {
        return answer;
    }

    public string GetHint()
    {
        return hint;
    }

    public Sprite GetHintImage()
    {
        return hintImage;
    }

    public Sprite GetQuestionImage()
    {
        return questionImage;
    }


    public string GetTitle()
    {
        return title;
    }
}
