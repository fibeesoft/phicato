﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GManager : MonoBehaviour {


    [SerializeField] Text txt_mainText, txt_title;
    [SerializeField] Image img_questionImage, img_hintImage;
    [SerializeField] InputField inp_inputAnswer;
    [SerializeField] GameObject go_InputFieldContainer, go_hintImageContainer, go_imageContainer, go_mainMenuContainer;
    [SerializeField] Button btn_nextTaskButton, btn_previousTaskButton;

    [SerializeField] GameObject go_questionContainer;
    [SerializeField] Text txt_question;

    [SerializeField] GameObject go_hintButtonContainer;
    [SerializeField] Button btn_hintButton;

    [SerializeField] GameObject go_lessonNumberContainer;
    [SerializeField] Text txt_lessonNumber;

    [SerializeField] GameObject go_hintContainer;
    [SerializeField] Text txt_hint;

    [Header("Module Lists")]
    [SerializeField] Task[] naukaList;
    [SerializeField] Task[] matematykaList;
    [SerializeField] Task[] chemiaList;
    [SerializeField] Task[] elektronikaList;
    Task[] taskList;
    Task[] emptyList;
    string[] playerPrefsArray;
    int taskNumber;
    string answer;
    string hint;
    Image hintImage;
    string module;
    string prefabName;
    bool isMenuOn;

    //TouchScreenKeyboard keyboard;

    private void Start()
    {
        playerPrefsArray = new string[] { "NaukaPref", "MatematykaPref", "ChemiaPref", "ElektronikaPref"};
        module = "menu";
        OpenMenu();
        ResetContainers();
        //keyboard.active = false;
        TouchScreenKeyboard.Open("TouchScreenKeyboardType.NumberPad");
     
    }

    public void SwitchPlayerPref(string prefName)
    {
        prefName = prefName + "Pref";
        prefabName = prefName;
        print(prefabName);
        if (PlayerPrefs.HasKey(prefabName))
        {
            taskNumber = PlayerPrefs.GetInt(prefabName);
            
        }
        else
        {
            taskNumber = 0;
            PlayerPrefs.SetInt(prefabName, taskNumber);
        }
    }

    

    public void ChooseModule(string moduleName)
    {
        print(moduleName);
        module = moduleName;
        SwitchModule(moduleName);
        ExitMenu();
        SwitchPlayerPref(moduleName);
        LoadPlayerProgress();
        GetTask();
    }

    public void SwitchModule(string moduleName)
    {
        switch (moduleName)
        {
            case "menu": print("menu");
                break;
            case "Nauka": taskList = naukaList;
                break;
            case "Matematyka": taskList = matematykaList;
                break;
            case "Chemia":
                taskList = chemiaList;
                break;
            case "Elektronika":
                taskList = elektronikaList;
                break;
            default: print("default");
                break;
            
        }
    }


    void GetTask()
    {
        ResetContainers();
        hint = taskList[taskNumber].GetHint();
        txt_question.text = taskList[taskNumber].GetQuestion();
        
        answer = taskList[taskNumber].GetAnswer();
        txt_lessonNumber.text = (taskNumber + 1).ToString() + " / " + taskList.Length;
        GetTitle();
        if(answer != "")
        {
            EnableInput();

            inp_inputAnswer.text = "";
        }

        if (hint != "")
        {
            EnableHintButton();
            txt_hint.text = taskList[taskNumber].GetHint();
        }

        if(taskList[taskNumber].GetQuestionImage() != null)
        {
            go_imageContainer.SetActive(true);
            img_questionImage.sprite = taskList[taskNumber].GetQuestionImage();
            txt_question.GetComponent<RectTransform>().sizeDelta = new Vector2(1050f, 800f);
        }
        else
        {
            go_imageContainer.SetActive(false);
            txt_question.GetComponent<RectTransform>().sizeDelta = new Vector2(1800f, 800f);
        }

        if(answer != "")
        {
            DisableNextButton();
        }
        else
        {
            EnableNextButton();
        }
    }


    public void NextTask()
    {
        if(taskNumber < taskList.Length - 1)
        {
            if (taskNumber < PlayerPrefs.GetInt(prefabName))
            {
                taskNumber++;
                GetTask();
            }
            else
            {
                taskNumber++;
                SavePlayerProgress();
                GetTask();
            }
        }
    }

    public void PreviousTask()
    {
        if(taskNumber > 0)
        {
            taskNumber--;
            GetTask();
        }

    }

    public void CheckAnswer()
    {
        if(inp_inputAnswer.text == answer || inp_inputAnswer.text == "z")
        {
            EnableNextButton();
            NextTask();
        }
        else
        {
            print("wrong answer");
        }
    }

    void DisableNextButton()
    {
        if(taskNumber == PlayerPrefs.GetInt(prefabName))
        {
            btn_nextTaskButton.interactable = false;
            print(taskNumber);
            print(PlayerPrefs.GetInt(prefabName));
        }
        else
        {
            btn_nextTaskButton.interactable = true;
        }
        
    }
    void EnableNextButton()
    {
        if((taskNumber < PlayerPrefs.GetInt(prefabName)) && module == "Matematyka")
        {
            btn_nextTaskButton.interactable = true;
            print(taskNumber);
            print(PlayerPrefs.GetInt(prefabName));
        }
        else
        {
            btn_nextTaskButton.interactable = true;
        }
        
    }

    void EnableHintButton()
    {
        go_hintButtonContainer.SetActive(true);
    }

    void DisableHintButton()
    {
        go_hintButtonContainer.SetActive(false);
    }

    void ResetContainers()
    {
        DisableHintButton();
        DisableInput();
    }

    void EnableInput()
    {
        go_InputFieldContainer.SetActive(true);
    }

    void DisableInput()
    {
        go_InputFieldContainer.SetActive(false);
    }

    public void GetHint()
    {
        go_hintImageContainer.SetActive(true);
        go_hintContainer.SetActive(true);
        if(taskList[taskNumber].GetHintImage() == null)
        {
            go_hintImageContainer.SetActive(false);
        }
        else
        {
            img_hintImage.sprite = taskList[taskNumber].GetHintImage();
        }
    }


    public void GetTitle()
    {
        if (taskList[taskNumber].GetTitle() == null)
        {

        }
        else
        {
            txt_title.text = taskList[taskNumber].GetTitle();
        }
    }

    public void CloseHint()
    {
        go_hintContainer.SetActive(false);
    }

    public void QuitApp()
    {
        Application.Quit();
    }

    public void ExitMenu()
    {
        isMenuOn = false;
        go_mainMenuContainer.SetActive(false);
    }

    public void OpenMenu()
    {
        isMenuOn = true;
        taskNumber = 0;
        taskList = emptyList;
        go_mainMenuContainer.SetActive(true);
    }

    public bool CheckIfMenuIsOn()
    {
        return isMenuOn;
    }

    private void SavePlayerProgress()
    {
        PlayerPrefs.SetInt(prefabName, taskNumber);
    }

    void LoadPlayerProgress()
    {
        taskNumber = PlayerPrefs.GetInt(prefabName);
    }

    public void ResetPlayerProgress()
    {
        foreach (var i in playerPrefsArray)
        {
            PlayerPrefs.SetInt(i, 0);
        }
        taskNumber = 0;
    }

    public void OpenURL()
    {
        Application.OpenURL("http://fibeesoft.com/");
    }



}

