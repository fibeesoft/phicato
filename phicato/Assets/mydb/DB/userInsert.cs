﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class userInsert : MonoBehaviour
{
    string URL = "http://localhost/mydb/userInsert.php";
    public string InputUsername, InputEmail;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            AddUser(InputUsername, InputEmail);
        }
    }

    public void AddUser(string username, string email)
    {
        WWWForm form = new WWWForm();
        form.AddField("addUsername", username);
        form.AddField("addEmail", email);

        WWW www = new WWW(URL, form);
    }
}
