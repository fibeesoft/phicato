﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class userUpdate : MonoBehaviour {

	string URL = "http://localhost/mydb/userUpdate.php";
	public string InputUsername, InputEmail, whereField, whereCondition;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            UpdateUser(InputUsername, InputEmail, whereField, whereCondition);
        }
    }

    public void UpdateUser(string username, string email, string wF, string wC)
    {
        WWWForm form = new WWWForm();
        form.AddField("editUsername", username);
        form.AddField("editEmail", email);
        form.AddField("whereField", wF);
        form.AddField("whereCondition", wC);

        WWW www = new WWW(URL, form);
    }
}
