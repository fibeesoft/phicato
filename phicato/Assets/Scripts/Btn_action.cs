﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Btn_action : MonoBehaviour
{
    [SerializeField] GameObject mng;
    bool isMenuOn;
    public void GetName()
    {
        isMenuOn = mng.GetComponent<Manager>().CheckIfMenuIsOn();
        if (isMenuOn)
        {
            mng.GetComponent<Manager>().TopicSwitcher(name);
        }
        else
        {
            print(this.name);
            mng.GetComponent<Manager>().CheckFourAnswersTask(this.name);
        }

    }

    
}
