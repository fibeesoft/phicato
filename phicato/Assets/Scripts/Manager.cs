﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    
    [SerializeField] GameObject[] UIGameObjects;
    [SerializeField] Text txt_header, txt_mainText;
    [SerializeField] Image img_mainImage;
    [SerializeField] Button[] btn_answerArray;
    [SerializeField] Page[] wholeArray, geometriaArray, obliczeniaArray, liczbyRzymskieArray;
    [SerializeField] InputField inp_answer;
    [SerializeField] AudioSource audioSorce;
    [SerializeField] Text txt_taskNumber, txt_pointsCounter, txt_result;
    [SerializeField] Image img_anim;
    [SerializeField] Button btn_solution;
    Animator anim;
    int taskNumberObliczenia, taskNumberLiczbyRzymskie, taskNumberGeometria;
    int pageNumber;
    int pointsCounter;
    string txt_solvedTask;
    bool isMenuOn;
    string topic;
    Page[] pageArray;
    string saveKeyName;
   

    private void Start()
    {
        topic = "";
        
        if (PlayerPrefs.HasKey("PPPointsCounter"))
        {
            pointsCounter = PlayerPrefs.GetInt("PPPointsCounter");
        }
        else
        {
            pointsCounter = 0;
            PlayerPrefs.SetInt("PPPointsCounter", pointsCounter);
        }
        txt_pointsCounter.text = "Zdobyte punkty: " + pointsCounter;
        anim = img_anim.GetComponent<Animator>();
        DisableAllGameObjects();
        OpenMenu();
    }

    void SaveProgress()
    {
        PlayerPrefs.SetInt(saveKeyName, pageNumber);
    }

    void LoadProgress(string subjectName)
    {
        if (PlayerPrefs.HasKey(subjectName))
        {
            pageNumber = PlayerPrefs.GetInt(subjectName);
            
        }
        else
        {
            pageNumber = 0;
            PlayerPrefs.SetInt(subjectName, pageNumber);
        }
    }

    

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            //StartCoroutine(PlayAnimation("Jump"));
            ShowSolution();
        }


        if (Input.GetKeyDown(KeyCode.Q))
        {
            foreach(var i in pageArray)
            {
                print(i.PageMainText());
            }
        }
    }

    public void TopicSwitcher(string topicParam)
    {
        topic = topicParam;

        switch (topic)
        {
            case "Obliczenia":
                saveKeyName = "PPrefObliczenia";
                LoadProgress(saveKeyName);
                print(topic);
                pageArray = obliczeniaArray;
                print(pageArray.Length);
                CloseMenu();
                LoadPage();
                break;
   
            case "LiczbyRzymskie":
                    saveKeyName = "PPrefLiczbyRzymskie";
                    LoadProgress(saveKeyName);
                    print(topic);
                    pageArray = liczbyRzymskieArray;
                    print(pageArray.Length);
                    CloseMenu();
                    LoadPage();
                    break;
                
            case "Geometria":
                saveKeyName = "PPrefGeometria";
                LoadProgress(saveKeyName);
                pageArray = geometriaArray;
                print(pageArray.Length);
                CloseMenu();
                LoadPage();
                break;
            case "Menu":
                {
                    saveKeyName = "PPrefMenu";
                    pageArray = null;
                    break;
                }
            default:
                break;
                
        }
    }

    IEnumerator PlayAnimation(string animName)
    {
        UIGameObjects[6].SetActive(true);
        if (animName == "Jump")
        {
             anim.Play(animName);
            yield return new WaitForSeconds(1);
            UIGameObjects[6].SetActive(false);
        }
        
    }


    void LoadPage()
    {
        txt_taskNumber.text = pageNumber.ToString();
        DisableAllGameObjects();
        UIGameObjects[7].SetActive(true);
        UIGameObjects[9].SetActive(true);
        if (pageArray[pageNumber].PageTitle() != "")
        {
            UIGameObjects[0].SetActive(true);
            txt_header.text = pageArray[pageNumber].PageTitle();
        }
        if(pageArray[pageNumber].PageMainText() != "")
        {
            UIGameObjects[1].SetActive(true);
            txt_mainText.text = pageArray[pageNumber].PageMainText();
        }
        if(pageArray[pageNumber].PageMainImage() != null)
        {
            UIGameObjects[2].SetActive(true);
            img_mainImage.sprite = pageArray[pageNumber].PageMainImage();
        }
        if(pageArray[pageNumber].PageIsFourAnswerTask() == true)
        {
            FourAnswersTask();
        }
        if(pageArray[pageNumber].PageIsInputAnswerTask() == true)
        {
            InputAnswerTask();
        }
        if(pageArray[pageNumber].PageSolvedTask() != "")
        {
            btn_solution.interactable = true;
            txt_solvedTask = pageArray[pageNumber].PageSolvedTask();
        }
        else
        {
            btn_solution.interactable = false;
        }

        
    }

    public void DisableAllGameObjects()
    {
        foreach (var i in UIGameObjects)
        {
            i.SetActive(false);
        }
    }

    void FourAnswersTask()
    {
        UIGameObjects[3].SetActive(true);

        int i = 0;
        foreach (var b in btn_answerArray)
        {
            Text txt = b.GetComponentInChildren<Text>();
            if (txt)
            {
                txt.text = pageArray[pageNumber].PageBtnAnswerTextArray(i);
                b.name = pageArray[pageNumber].PageBtnAnswerTextArray(i);
                i++;
            }
        }
    }

    public void CheckFourAnswersTask(string i)
    {
        if(pageArray[pageNumber].PageAnswer() == i)
        {
            print("Dobra odpowiedz");
            PlayGoodAnswerSound();
            pointsCounter++;
            PlayerPrefs.SetInt("PPPointsCounter", pointsCounter);
            txt_pointsCounter.text = "Zdobyte punkty: " + pointsCounter;
            OpenNextPage();
            SaveProgress();
        }
    }

    void InputAnswerTask()
    {
        UIGameObjects[4].SetActive(true);
    }

    public void CheckInputAnswerTask()
    {
        if(inp_answer.text == pageArray[pageNumber].PageAnswer())
        {
            inp_answer.text = "";
            PlayGoodAnswerSound();
            pointsCounter++;
            PlayerPrefs.SetInt("PPPointsCounter", pointsCounter);
            txt_pointsCounter.text = "Zdobyte punkty: " + pointsCounter;
            OpenNextPage();
            SaveProgress();
        }
        else
        {
            inp_answer.text = "";
            print("sprobuj ponownie");
        }
    }

    public void OpenNextPage()
    {
        if(pageNumber < pageArray.Length-1)
        {
            pageNumber++;
            LoadPage();
        }
    }

    public void OpenPrevPage()
    {
        if(pageNumber > 0)
        {
            pageNumber--;
            LoadPage();
        }
    }

    void PlayGoodAnswerSound()
    {
        audioSorce.Play();
        
    }

    public void StartLesson()
    {
        UIGameObjects[5].SetActive(false);
        LoadPage();

    }

    public void PPClear()
    {
        PlayerPrefs.DeleteAll();
        pointsCounter = 0;
        txt_pointsCounter.text = "Zdobyte punkty: " + pointsCounter;
        DisableAllGameObjects();
        OpenMenu();
    }

    public void OpenMenu()
    {
        isMenuOn = true;
        DisableAllGameObjects();
        UIGameObjects[5].SetActive(true);
        TopicSwitcher("Menu");
    }

    public void CloseMenu()
    {
        isMenuOn = false;
        UIGameObjects[5].SetActive(false);
    }


    public bool CheckIfMenuIsOn()
    {
        return isMenuOn;
    }




    public void ShowSolution()
    {
        UIGameObjects[8].SetActive(true);
        UIGameObjects[8].GetComponentInChildren<Text>().text = txt_solvedTask;
        txt_result.text = pageArray[pageNumber].PageAnswer();
    }

    public void CloseSolutionPanel()
    {
        UIGameObjects[8].SetActive(false);
    }
}
