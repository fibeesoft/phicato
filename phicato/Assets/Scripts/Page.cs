﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Page")]
public class Page : ScriptableObject
{
    [SerializeField] string title;
    [SerializeField] Sprite mainImage;
    [TextArea(6, 8)] [SerializeField] string mainText;
    [SerializeField] bool isFourAnswerTask;
    [SerializeField] string[] btnAnswerTextArray;
    [SerializeField] Sprite[] btnAnswerSpriteArray;
    [SerializeField] bool isInputAnswerTask;
    [SerializeField] string answer;
    [SerializeField] string hint;
    [TextArea(6, 8)]  [SerializeField] string solvedTask;
    [SerializeField] Sprite solvedTaskImage;

    public string PageBtnAnswerTextArray(int i)
    {
        return btnAnswerTextArray[i];
    }

    public string PageTitle()
    {
        return title;
    }

    public string PageMainText()
    {
        return mainText;
    }

    public Sprite PageMainImage()
    {
        return mainImage;
    }

    public bool PageIsFourAnswerTask()
    {
        return isFourAnswerTask;
    }

    public bool PageIsInputAnswerTask()
    {
        return isInputAnswerTask;
    }

    public string PageAnswer()
    {
        return answer;
    }

    public string PageHInt()
    {
        return hint;
    }

    public string PageSolvedTask()
    {
        return solvedTask;
    }

    public Sprite PageSolvedTaskImage()
    {
        return solvedTaskImage;
    }

}
